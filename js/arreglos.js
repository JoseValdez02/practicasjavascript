// Generar números aleatorios
function generarNumeros(cantidad) {
  var numeros = [];
  for (var i = 0; i < cantidad; i++) {
      numeros.push(Math.floor(Math.random() * 100)); // Genera números aleatorios entre 0 y 100
  }
  return numeros;
}

// Mostrar los elementos de un array
function mostrarElementos(array) {
  var lblElementos = document.getElementById("lblElementos");
  lblElementos.innerHTML = array.join(", ");
}

// Mostrar los números pares e impares y su porcentaje
function mostrarNumerosPares(array) {
  var lblNumerosPares = document.getElementById("lblNumerosPares");
  var numerosPares = array.filter(function(num) {
      return num % 2 === 0;
  });

  var numerosImpares = array.filter(function(num) {
      return num % 2 !== 0;
  });

  var porcentajePares = (numerosPares.length / array.length) * 100;
  var porcentajeImpares = (numerosImpares.length / array.length) * 100;

  lblNumerosPares.innerHTML = "Números pares: " + numerosPares.join(", ") + ", Porcentaje de pares: " + porcentajePares.toFixed(2) + "%" + "<br>" +
      "Números impares: " + numerosImpares.join(", ") + ", Porcentaje de impares: " + porcentajeImpares.toFixed(2) + "%";
}

// Mostrar el valor mayor y su posición
function mostrarMayor(array) {
  var mayor = array[0];
  var posicion = 0;
  for (var i = 1; i < array.length; i++) {
      if (array[i] > mayor) {
          mayor = array[i];
          posicion = i;
      }
  }
  var lblMayor = document.getElementById("lblMayor");
  lblMayor.innerHTML = "Valor mayor: " + mayor + ", Posición: " + posicion;
}

// Mostrar el promedio de un arreglo
function mostrarPromedio(array) {
  var suma = array.reduce(function(acc, num) {
      return acc + num;
  }, 0);
  var promedio = suma / array.length;
  var lblPromedio = document.getElementById("lblPromedio");
  lblPromedio.innerHTML = "Promedio: " + promedio.toFixed(2);
}

// Mostrar el valor menor y su posición
function mostrarMenor(array) {
  var menor = array[0];
  var posicion = 0;
  for (var i = 1; i < array.length; i++) {
      if (array[i] < menor) {
          menor = array[i];
          posicion = i;
      }
  }
  var lblMenor = document.getElementById("lblMenor");
  lblMenor.innerHTML = "Valor menor: " + menor + ", Posición: " + posicion;
}

// Mostrar el porcentaje de simetría de los números aleatorios
function mostrarPorcentajeSimetria(array) {
  var simetricos = 0;
  for (var i = 0; i < array.length / 2; i++) {
      if (array[i] === array[array.length - 1 - i]) {
          simetricos++;
      }
  }
  var porcentaje = (simetricos / array.length) * 100;
  var lblSimetria = document.getElementById("lblSimetria");
  lblSimetria.innerHTML = "Porcentaje de simetría: " + porcentaje + "%";
}

// Función para generar números y mostrarlos en el select
function Generar() {
  var cantidadNumeros = parseInt(document.getElementById("txtNumeros").value);
  var numerosGenerados = generarNumeros(cantidadNumeros);
  mostrarElementos(numerosGenerados);
  mostrarNumerosPares(numerosGenerados);
  mostrarMayor(numerosGenerados);
  mostrarPromedio(numerosGenerados);
  mostrarMenor(numerosGenerados);
  mostrarPorcentajeSimetria(numerosGenerados);
}
