// Obtener el objeto button de calcular
const btnCalcularIMC = document.getElementById('btnCalcularIMC');
btnCalcularIMC.addEventListener('click',function(){

let Peso = document.getElementById('txtPeso').value;
let Altura = document.getElementById('txtAltura').value;
let Edad = document.getElementById('txtEdad').value;

// HACER LOS CALCULOS
let IMC = Peso / (Altura * Altura);
let Calorias;

// MOSTRAR RESULTADO

document.getElementById('txtIMC').value = IMC.toFixed(2);
cargaImagen(IMC, Edad, Peso);

});

function cargaImagen(IMC, Edad, Peso){
    let img = document.getElementById('imagen');
    let sexo = document.querySelector('input[name="sexo"]:checked').value;
    let Calorias;
   
    if(sexo === "sexoMasculino") {
    if(IMC<18.5) {
        img.src ="/img/01H.png";
} else if(IMC<24.9) {
        img.src ="/img/02H.png";
} else if(IMC<29.9) {
        img.src ="/img/03H.png";
} else if(IMC<34.9) {
        img.src ="/img/04H.png";
} else if(IMC<39.9) {
        img.src ="/img/05H.png";
} else if(IMC>40) {
        img.src ="/img/06H.png";
} 

if (Edad >= 10 && Edad < 18){
        Calorias = 17.686 * Peso + 658.2;
} else if (Edad >= 18 && Edad < 30){
        Calorias = 15.057 * Peso + 692.2;
} else if (Edad >= 30 && Edad < 60){
        Calorias = 11.472 * Peso + 873.1;
} else if (Edad >= 60){
        Calorias = 11.711 * Peso + 587.1;
}

    } else {
        if(IMC<18.5) {
                img.src ="/img/01M.png";
        } else if(IMC<24.9) {
                img.src ="/img/02M.png";
        } else if(IMC<29.9) {
                img.src ="/img/03M.png";
        } else if(IMC<34.9) {
                img.src ="/img/04M.png";
        } else if(IMC<39.9) {
                img.src ="/img/05M.png";
        } else if(IMC>40) {
                img.src ="/img/06M.png";
        }  

        if (Edad >= 10 && Edad < 18) {
                Calorias = 13.384 * Peso + 692.6;
        } else if (Edad >= 18 && Edad < 30){
                Calorias = 14.818 * Peso + 486.6;
        } else if (Edad >= 30 && Edad < 60){
                Calorias = 8.126 * Peso + 845.6;
        } else if (Edad >= 60){
                Calorias = 9.082 * Peso + 658.5;
        }
    }

    document.getElementById('txtCalorias').value = Calorias.toFixed(2);
}